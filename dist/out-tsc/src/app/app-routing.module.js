import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { UserComponent } from './user/user.component';
import { AddclientComponent } from './addclient/addclient.component';
import { AdduserComponent } from './adduser/adduser.component';
import { AuthGuard } from './auth.guard';
import { DriveComponent } from './drive/drive.component';
var routes = [
    { path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent },
    { path: '', component: LoginComponent },
    { path: 'user', canActivate: [AuthGuard], component: UserComponent },
    { path: 'client', canActivate: [AuthGuard], component: ClientComponent },
    { path: 'addclient', canActivate: [AuthGuard], component: AddclientComponent },
    { path: 'adduser', canActivate: [AuthGuard], component: AdduserComponent },
    { path: 'drive', canActivate: [AuthGuard], component: DriveComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map