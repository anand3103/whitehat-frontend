import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
var ClientComponent = /** @class */ (function () {
    function ClientComponent(userServices) {
        this.userServices = userServices;
        this.checkboxValue = false;
    }
    ClientComponent.prototype.ngOnInit = function () {
        $('#example').DataTable();
        this.getAllData();
    };
    ClientComponent.prototype.getAllData = function () {
        var _this = this;
        this.userServices.getAllClients().subscribe(function (res) {
            _this.allData = res['data'];
            console.log('response', _this.allData);
        });
    };
    ClientComponent.prototype.updateClient = function (id, status) {
        console.log('id status', id, status);
        if (status == 1) {
            this.checkboxValue = true;
        }
        if (status == 0) {
            this.checkboxValue = false;
        }
        this.userServices.updateClient(status, id)
            .subscribe(function (res) {
            console.log('response', res);
        });
    };
    ClientComponent = tslib_1.__decorate([
        Component({
            selector: 'app-client',
            templateUrl: './client.component.html',
            styleUrls: ['./client.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService])
    ], ClientComponent);
    return ClientComponent;
}());
export { ClientComponent };
//# sourceMappingURL=client.component.js.map