import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.userType = JSON.parse(localStorage.getItem("userLogin"));
        console.log('usertype dashboard', this.userType);
        this.pieChart();
        this.columnChart();
        $('#bar-panel-fullscreen').click(function (e) {
            $('#bar-chart').toggleClass('fullscreen');
        });
        $('#pie-panel-fullscreen').click(function (e) {
            $('#pie-chart').toggleClass('fullscreen');
        });
    };
    DashboardComponent.prototype.pieChart = function () {
        this.type = 'PieChart';
        this.data = [
            ['Firefox', 45.0],
            ['IE', 26.8],
            ['Chrome', 12.8],
            ['Safari', 8.5],
            ['Opera', 6.2],
            ['Others', 0.7]
        ];
        this.columnNames = ['Browser', 'Percentage'];
        this.options = {};
        this.width = 550;
        this.height = 300;
    };
    DashboardComponent.prototype.columnChart = function () {
        this.type2 = 'ColumnChart';
        this.data2 = [
            ["2012", 900, 390],
            ["2013", 1000, 400],
            ["2014", 1170, 440],
            ["2015", 1250, 480],
            ["2016", 1530, 540]
        ];
        this.columnNames2 = ['Year', 'Asia', 'Europe'];
        this.options2 = {};
        this.width2 = 1000;
        this.height2 = 400;
    };
    DashboardComponent = tslib_1.__decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map