import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(toastr, router, http, _fb) {
        this.toastr = toastr;
        this.router = router;
        this.http = http;
        this._fb = _fb;
        this.submitted = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.userFormData();
        console.log('this.', this.f);
    };
    LoginComponent.prototype.userLogin = function () {
        // this.submitted = true;
        var _this = this;
        // if (this.g.value.Email && this.g.value.Password) {
        // this.submitted = false;
        // var userData ={
        //   // "Email":"rohan@connexistech.com",
        //   // "Password":"123456"
        //   "Email":this.g.value.Email,
        //   "Password":this.g.value.Password
        // }
        return this.http.post('http://localhost:3000/api/login', this.g.value)
            .subscribe(function (res) {
            if (res['status'] == "200") {
                localStorage.setItem("userLogin", JSON.stringify(res['data']));
                _this.router.navigate(['dashboard']);
                console.log('gettingh user here', res['data'].Usertype == "1");
            }
            else {
                console.log('Your account', res);
            }
        });
    };
    LoginComponent.prototype.userFormData = function () {
        this.userForm = this._fb.group({
            Email: ['', Validators.compose([Validators.required, Validators.email])],
            Password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () { return this.userForm.controls; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "g", {
        get: function () { return this.userForm; },
        enumerable: true,
        configurable: true
    });
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [ToastrService, Router, HttpClient, FormBuilder])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map