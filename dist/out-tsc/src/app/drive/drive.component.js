import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormBuilder } from '@angular/forms';
var DriveComponent = /** @class */ (function () {
    function DriveComponent(userService, _fb) {
        this.userService = userService;
        this._fb = _fb;
    }
    DriveComponent.prototype.ngOnInit = function () {
        this.userType = JSON.parse(localStorage.getItem("userLogin"));
        console.log("company_name id:", this.userType.Id);
        this.getCompanyName();
        this.doctorFormData();
    };
    DriveComponent.prototype.createDirectory = function () {
        console.log("folderName", this.foldername);
        this.userService.createDirectory(this.companyname, this.foldername);
    };
    DriveComponent.prototype.getCompanyName = function () {
        var _this = this;
        this.userService.getCompanyName(this.userType.Id)
            .subscribe(function (res) {
            _this.companyname = res['data'];
            console.log("hii tis is harshal", _this.companyname);
        });
    };
    DriveComponent.prototype.handleFileChanged = function (event) {
        // this.file = event.target.files[0];
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.addImage.get('profile').setValue(file);
            // this.addImage.get('profile').setValue(file);
            console.log('add inamgekasjdlfjsdafasdf', this.addImage);
        }
        // var formData: any = new FormData();
        // formData.append("file", this.file, this.file.name);
        // formData.append('data', this.userType.Id);
        // var file = {
        //   file: this.file,
        //   filename: this.file.name
        // }
        // console.log('file formData', formData);
        this.onSubmit();
    };
    DriveComponent.prototype.onSubmit = function () {
        var formData = new FormData();
        formData.append('file', this.addImage.value.profile);
        // formData.append('files', this.addImage.get('profile').value);
        // formData.append('Id', this.userType.Id);
        // console.log('file formData', formData);
        this.userService.fileUpload(formData, this.userType.Id)
            .subscribe(function (res) {
            console.log('file response', res);
        });
    };
    DriveComponent.prototype.doctorFormData = function () {
        this.addImage = this._fb.group({
            profile: [''],
        });
    };
    DriveComponent = tslib_1.__decorate([
        Component({
            selector: 'app-drive',
            templateUrl: './drive.component.html',
            styleUrls: ['./drive.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService, FormBuilder])
    ], DriveComponent);
    return DriveComponent;
}());
export { DriveComponent };
//# sourceMappingURL=drive.component.js.map