import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';
var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type");
var httpOptions = {
    headers: new HttpHeaders({
        'enctype': 'multipart/form-data',
        'Accept': 'application/json'
    })
};
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.a = '';
    }
    UserService.prototype.Login = function () {
        console.log('inside servicec');
        var userData = {
            "Email": "rohan@connexistech.com",
            "Password": "123456"
        };
        return this.http.post(environment.apiUrl + 'login', userData)
            .subscribe(function (res) {
            console.log('res', res);
            return res;
        });
    };
    UserService.prototype.addClient = function (client) {
        console.log("service data", client);
        return this.http.post(environment.apiUrl + 'addClient', client)
            .subscribe(function (res) {
            console.log('res', res);
        });
    };
    UserService.prototype.addUser = function (user) {
        console.log("service data", user);
        return this.http.post(environment.apiUrl + 'addUser', user)
            .subscribe(function (res) {
            console.log('res', res);
        });
    };
    UserService.prototype.getAllClients = function () {
        console.log('inside service get all clients');
        return this.http.get(environment.apiUrl + 'getAllClients/' + '2')
            .pipe(map(function (res) {
            console.log('repsionse client', res);
            return res;
        }));
    };
    UserService.prototype.getAllusers = function () {
        return this.http.get(environment.apiUrl + 'getAllClients/' + '3')
            .pipe(map(function (res) {
            return res;
        }));
    };
    UserService.prototype.updateUser = function (check, id) {
        var userId = {
            check: check,
            Id: id
        };
        console.log('inside service', userId);
        return this.http.put(environment.apiUrl + 'updateUser/' + check + '/' + id, '')
            .pipe(map(function (res) {
            return res;
        }));
    };
    UserService.prototype.updateClient = function (check, id) {
        var userId = {
            check: check,
            Id: id
        };
        console.log('**********inside service*****', userId);
        return this.http.put(environment.apiUrl + 'updateClient/' + check + '/' + id, '')
            .pipe(map(function (res) {
            return res;
        }));
    };
    UserService.prototype.createDirectory = function (company, foldername) {
        return this.http.post(environment.apiUrl + 'createDirectory/' + company + '/' + foldername, '')
            .subscribe(function (res) {
            console.log('res', res);
        });
    };
    UserService.prototype.getCompanyName = function (Id) {
        console.log('inside service', Id);
        return this.http.get(environment.apiUrl + 'getCompanyName/' + Id)
            .pipe(map(function (res) {
            return res;
        }));
    };
    UserService.prototype.fileUpload = function (file, Id) {
        // console.log('inside data',file)
        // console.log('inside data Id',Id)
        return this.http.post(environment.apiUrl + 'fileUpload/' + file, Id)
            .pipe(map(function (res) {
            return res;
        }));
    };
    UserService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], UserService);
    return UserService;
}());
export { UserService };
//# sourceMappingURL=user.service.js.map