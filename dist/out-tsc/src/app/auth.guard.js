import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
var AuthGuard = /** @class */ (function () {
    function AuthGuard(routes) {
        this.routes = routes;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var user = JSON.parse(localStorage.getItem("userLogin"));
        // if(user !== null){
        if (user !== null) {
            console.log('username', localStorage.getItem("userLogin"));
            return true;
        }
        else {
            this.routes.navigate(['']);
            return false;
        }
    };
    AuthGuard = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], AuthGuard);
    return AuthGuard;
}());
export { AuthGuard };
//# sourceMappingURL=auth.guard.js.map