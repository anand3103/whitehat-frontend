import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { UserComponent } from './user/user.component';
import { AddclientComponent } from './addclient/addclient.component';
import { AdduserComponent } from './adduser/adduser.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiSwitchModule } from 'ngx-toggle-switch';
// import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './auth.guard';
import { UserService } from './services/user.service';
import { DriveComponent } from './drive/drive.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                AppComponent,
                DashboardComponent,
                HeaderComponent,
                TopHeaderComponent,
                LoginComponent,
                ClientComponent,
                UserComponent,
                AddclientComponent,
                AdduserComponent,
                DriveComponent
            ],
            imports: [
                BrowserModule,
                HttpClientModule,
                GoogleChartsModule,
                ReactiveFormsModule,
                FormsModule,
                AppRoutingModule,
                BrowserAnimationsModule,
                ToastrModule.forRoot(),
                UiSwitchModule,
            ],
            providers: [
                AuthGuard,
                UserService
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map