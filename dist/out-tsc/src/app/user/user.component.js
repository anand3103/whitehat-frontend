import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
var UserComponent = /** @class */ (function () {
    function UserComponent(userServices) {
        this.userServices = userServices;
        this.checkboxValue = false;
    }
    UserComponent.prototype.ngOnInit = function () {
        $('#example').DataTable();
        this.getAllData();
    };
    UserComponent.prototype.getAllData = function () {
        var _this = this;
        this.userServices.getAllusers().subscribe(function (res) {
            _this.allData = res['data'];
            console.log('response', _this.allData);
        });
    };
    UserComponent.prototype.updateUser = function (id, status) {
        console.log('id status', id, status);
        var userId;
        if (status == 1) {
            this.checkboxValue = true;
        }
        if (status == 0) {
            this.checkboxValue = false;
        }
        this.userServices.updateUser(status, id)
            .subscribe(function (res) {
            console.log('response', res);
        });
    };
    UserComponent = tslib_1.__decorate([
        Component({
            selector: 'app-user',
            templateUrl: './user.component.html',
            styleUrls: ['./user.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService])
    ], UserComponent);
    return UserComponent;
}());
export { UserComponent };
//# sourceMappingURL=user.component.js.map