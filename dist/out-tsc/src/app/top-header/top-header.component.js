import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var TopHeaderComponent = /** @class */ (function () {
    function TopHeaderComponent(router) {
        this.router = router;
    }
    TopHeaderComponent.prototype.ngOnInit = function () {
        this.userType = JSON.parse(localStorage.getItem("userLogin"));
        console.log('top-header', this.userType);
    };
    TopHeaderComponent.prototype.singOut = function () {
        localStorage.removeItem("userLogin");
        console.log('localStorage.removeItem("user");', localStorage.removeItem("userLogin"));
        this.router.navigate(['']);
        // this.toastr.success("Logout");
    };
    TopHeaderComponent = tslib_1.__decorate([
        Component({
            selector: 'app-top-header',
            templateUrl: './top-header.component.html',
            styleUrls: ['./top-header.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], TopHeaderComponent);
    return TopHeaderComponent;
}());
export { TopHeaderComponent };
//# sourceMappingURL=top-header.component.js.map