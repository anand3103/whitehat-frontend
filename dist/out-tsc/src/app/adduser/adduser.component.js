import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
var AdduserComponent = /** @class */ (function () {
    function AdduserComponent(formBuilder, userService) {
        this.formBuilder = formBuilder;
        this.userService = userService;
    }
    AdduserComponent.prototype.ngOnInit = function () {
        this.userType = JSON.parse(localStorage.getItem("userLogin"));
        this.registerForm = this.formBuilder.group({
            companyname: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            mobile: ['', Validators.required],
            password: ['', Validators.required],
            reads: ['', Validators.required],
            writes: ['', Validators.required]
        });
        console.log("msadfjslakdfsjad", this.registerForm.value);
    };
    AdduserComponent.prototype.onSubmit = function () {
        if (this.registerForm.value.reads == true) {
            this.registerForm.value.read = 1;
        }
        else {
            this.registerForm.value.read = 0;
        }
        if (this.registerForm.value.writes == true) {
            this.registerForm.value.write = 1;
        }
        else {
            this.registerForm.value.write = 0;
        }
        // console.log("data",this.registerForm.value);
        this.userService.addUser(this.registerForm.value);
    };
    AdduserComponent = tslib_1.__decorate([
        Component({
            selector: 'app-adduser',
            templateUrl: './adduser.component.html',
            styleUrls: ['./adduser.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder, UserService])
    ], AdduserComponent);
    return AdduserComponent;
}());
export { AdduserComponent };
//# sourceMappingURL=adduser.component.js.map