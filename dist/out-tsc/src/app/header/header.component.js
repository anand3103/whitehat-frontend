import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.userType = JSON.parse(localStorage.getItem("userLogin"));
        console.log('header', this.userType);
        $(".xn-openable").click(function () {
            $(".xn-inside").toggleClass("h-auto");
            console.log("click");
        });
        $(".xn-openable2").click(function () {
            $(".xn-inside2").toggleClass("h-auto");
            console.log("click");
        });
        $(".xn-openable3").click(function () {
            $(".xn-inside3").toggleClass("h-auto");
            console.log("click");
        });
        $(".xn-openable4").click(function () {
            $(".xn-inside4").toggleClass("h-auto");
            console.log("click");
        });
    };
    HeaderComponent = tslib_1.__decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map