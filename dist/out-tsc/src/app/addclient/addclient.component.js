import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
var AddclientComponent = /** @class */ (function () {
    function AddclientComponent(formBuilder, userService) {
        this.formBuilder = formBuilder;
        this.userService = userService;
    }
    AddclientComponent.prototype.ngOnInit = function () {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            mobile: ['', Validators.required],
            password: ['', Validators.required],
        });
    };
    AddclientComponent.prototype.onSubmit = function () {
        // console.log("data",this.registerForm.value);
        this.userService.addClient(this.registerForm.value);
    };
    AddclientComponent = tslib_1.__decorate([
        Component({
            selector: 'app-addclient',
            templateUrl: './addclient.component.html',
            styleUrls: ['./addclient.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder, UserService])
    ], AddclientComponent);
    return AddclientComponent;
}());
export { AddclientComponent };
//# sourceMappingURL=addclient.component.js.map