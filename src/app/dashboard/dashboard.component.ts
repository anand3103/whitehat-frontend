import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

declare const $;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  type:any;
  data:any;
  columnNames:any;
  options:any;
  width:any;
  height:any;

  type2:any;
  data2:any;
  columnNames2:any;
  options2:any;
  width2:any;
  height2:any;
  userType:any;


  constructor() { }

  ngOnInit() {

    this.userType = JSON.parse(localStorage.getItem("userLogin"))
    console.log('usertype dashboard', this.userType);
    this.pieChart();
    this.columnChart();
    $('#bar-panel-fullscreen').click(function(e){
      $('#bar-chart').toggleClass('fullscreen'); 
    });
    $('#pie-panel-fullscreen').click(function(e){
      $('#pie-chart').toggleClass('fullscreen'); 
    });
  }
  
  pieChart(){
    this.type = 'PieChart';
    this.data = [
        ['Firefox', 45.0],
        ['IE', 26.8],
        ['Chrome', 12.8],
        ['Safari', 8.5],
        ['Opera', 6.2],
        ['Others', 0.7] 
    ];
    this.columnNames = ['Browser', 'Percentage'];
    this.options = {    
    };
    this.width = 550;
    this.height = 300;
  }

  columnChart(){
    this.type2 = 'ColumnChart';
    this.data2 = [
        ["2012", 900, 390],
        ["2013", 1000, 400],
        ["2014", 1170, 440],
        ["2015", 1250, 480],
        ["2016", 1530, 540]
    ];
    this.columnNames2 = ['Year', 'Asia','Europe'];
    this.options2 = {};
    this.width2 = 1000;
    this.height2 = 400;
  }

}
