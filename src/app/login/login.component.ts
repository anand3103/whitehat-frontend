import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type");



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  submitted = false;
  userForm: any;

  userInfo: any;

  constructor(private toastr: ToastrService, private router: Router, private http: HttpClient, private _fb: FormBuilder) { }

  ngOnInit() {
    this.userFormData();
    console.log('this.', this.f)
  }

  userLogin() {
    // this.submitted = true;

    // if (this.g.value.Email && this.g.value.Password) {
    // this.submitted = false;
    // var userData ={
    //   // "Email":"rohan@connexistech.com",
    //   // "Password":"123456"

    //   "Email":this.g.value.Email,
    //   "Password":this.g.value.Password
    // }
    return this.http.post('http://localhost:3000/api/login', this.g.value)
      .subscribe(res => {
        if (res['status'] == "200") {
          localStorage.setItem("userLogin", JSON.stringify(res['data']));
          this.router.navigate(['dashboard']);
          console.log('gettingh user here', res['data'].Usertype == "1");
        }else{
          console.log('Your account',res);
        }
       
      })

  }

  userFormData() {
    this.userForm = this._fb.group({
      Email: ['', Validators.compose([Validators.required, Validators.email])],
      Password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
    })
  }
  get f() { return this.userForm.controls; }
  get g() { return this.userForm }
}
