import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
declare const $;

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  data: any;
  allData: any;
  checkboxValue:boolean = false;
  constructor(public userServices: UserService) { }

  ngOnInit() {
    $('#example').DataTable();
    this.getAllData();
  }


  getAllData() {
    
    this.userServices.getAllClients().subscribe(
      res => {
        this.allData = res['data'];
        console.log('response', this.allData);
      }
    )
  }

  updateClient(id, status){

    console.log('id status', id, status)
    
    if(status == 1){
      this.checkboxValue  = true
    }
    if(status == 0){
      this.checkboxValue = false
    }
    this.userServices.updateClient(status, id)
      .subscribe(res =>{
        console.log('response', res)
      })
  }
 
}
