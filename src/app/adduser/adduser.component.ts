import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  
  userType:any;
  registerForm: FormGroup;
  
  constructor(private formBuilder: FormBuilder, private userService:UserService) { }

  ngOnInit() {
    this.userType = JSON.parse(localStorage.getItem("userLogin"))
    this.registerForm = this.formBuilder.group({
      companyname: ['', Validators.required],  
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', Validators.required],
      password: ['',Validators.required],
      reads:['',Validators.required],
      writes:['',Validators.required]

  });
  console.log("msadfjslakdfsjad",this.registerForm.value);

  }

  onSubmit(){
    
    if(this.registerForm.value.reads == true){
      this.registerForm.value.read  = 1
    }else{
      this.registerForm.value.read  = 0
    }
    if(this.registerForm.value.writes == true){
      this.registerForm.value.write = 1
    }else{
      this.registerForm.value.write  = 0;
    }

    // console.log("data",this.registerForm.value);
    this.userService.addUser(this.registerForm.value);
    
  }

}
