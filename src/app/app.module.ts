import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { UserComponent } from './user/user.component';
import { AddclientComponent } from './addclient/addclient.component';
import { AdduserComponent } from './adduser/adduser.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiSwitchModule } from 'ngx-toggle-switch';


// import { AppRoutingModule } from './app-routing.module';

import { AuthGuard } from './auth.guard';
import { UserService } from './services/user.service';
import { from } from 'rxjs';
import { DriveComponent } from './drive/drive.component';
import { CommonComponent } from './common/common.component';
import { RightbarComponent } from './rightbar/rightbar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    TopHeaderComponent,
    LoginComponent,
    ClientComponent,
    UserComponent,
    AddclientComponent,
    AdduserComponent,
    DriveComponent,
    CommonComponent,
    RightbarComponent
  ],
  imports: [
    BrowserModule,  
    HttpClientModule,
    GoogleChartsModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    UiSwitchModule,
  ],
  providers: [
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
