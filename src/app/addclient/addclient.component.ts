import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-addclient',
  templateUrl: './addclient.component.html',
  styleUrls: ['./addclient.component.css']
})
export class AddclientComponent implements OnInit {

  registerForm: FormGroup;


  constructor(private formBuilder:FormBuilder, private userService:UserService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', Validators.required],
      password: ['',Validators.required],
  });
  }

  onSubmit(){
    // console.log("data",this.registerForm.value);
    this.userService.addClient(this.registerForm.value);
    
  }

}
