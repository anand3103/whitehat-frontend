import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css']
})
export class TopHeaderComponent implements OnInit {

  userType:any;

  constructor(private router:Router) { }

  ngOnInit() {

    this.userType = JSON.parse(localStorage.getItem("userLogin"))
    console.log('top-header', this.userType);

  }

  singOut(){
    localStorage.removeItem("userLogin");
    console.log('localStorage.removeItem("user");',localStorage.removeItem("userLogin"))
    this.router.navigate(['']);

    // this.toastr.success("Logout");
    }

}
