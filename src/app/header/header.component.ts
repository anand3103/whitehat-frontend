import { Component, OnInit } from '@angular/core';

declare const $;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userType:any;

  constructor() { }

  ngOnInit() {

    this.userType = JSON.parse(localStorage.getItem("userLogin"))
    console.log('header', this.userType);
    $(".xn-openable").click(function(){
      $(".xn-inside").toggleClass("h-auto");
      console.log("click")
    });
    $(".xn-openable2").click(function(){
      $(".xn-inside2").toggleClass("h-auto");
      console.log("click")
    });
    $(".xn-openable3").click(function(){
      $(".xn-inside3").toggleClass("h-auto");
      console.log("click")
    });
    $(".xn-openable4").click(function(){
      $(".xn-inside4").toggleClass("h-auto");
      console.log("click")
    });
  }

}
