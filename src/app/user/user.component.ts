import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

declare const $;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  
  data: any;
  allData: any;
  checkboxValue:boolean = false;
  constructor(public userServices: UserService) { }

  ngOnInit() {
    $('#example').DataTable();
    this.getAllData();
  }

  getAllData() {
    this.userServices.getAllusers().subscribe(
      res => {
        this.allData = res['data'];
        console.log('response', this.allData);
      }
    )
  }
  

  updateUser(id, status){

    console.log('id status', id, status)
    var userId;
    if(status == 1){
      this.checkboxValue  = true
    }
    if(status == 0){
      this.checkboxValue = false
    }
    this.userServices.updateUser(status, id)
      .subscribe(res =>{
        console.log('response', res)
      })
  }
  // changeAction(event){
  //   console.log('tagetted vel',event.target.value)
  // }

}
