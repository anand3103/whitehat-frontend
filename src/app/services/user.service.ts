import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';



var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type");

const httpOptions = {
  headers: new HttpHeaders({
    'enctype': 'multipart/form-data',
    'Accept': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

public a :any = '';
  constructor(private http: HttpClient) { }

   Login() {
     console.log('inside servicec');
     
    var userData ={
      "Email":"rohan@connexistech.com",
      "Password":"123456"
    }
    return this.http.post(environment.apiUrl + 'login', userData)
      .subscribe(res => {
        console.log('res', res)
        return res;
      })
  }r

  addClient(client){
      console.log("service data",client);
      return this.http.post(environment.apiUrl + 'addClient', client)
      .subscribe(res => {
        console.log('res', res)
      })
  }

  addUser(user){
    console.log("service data",user);
    return this.http.post(environment.apiUrl + 'addUser', user)
    .subscribe(res => {
      console.log('res', res)
    })
}

getAllClients(){ 
  console.log ('inside service get all clients')
  return this.http.get(environment.apiUrl + 'getAllClients/'+ '2')
        .pipe(
            map(res=>{
              console.log('repsionse client', res)
              return res;
            }))

}


getAllusers(){ 
  return this.http.get(environment.apiUrl + 'getAllClients/'+ '3')
        .pipe(
            map(res=>{
              return res
            }))

}

  updateUser(check, id){
    let userId = {
      check: check,
      Id: id
    }
    console.log('inside service', userId)
    return this.http.put(environment.apiUrl + 'updateUser/'+ check+'/'+id,'')
    .pipe(
      map(res=>{
          return res
        }))
  }


  updateClient(check, id){
    let userId = {
      check: check,
      Id: id
    }
    console.log('**********inside service*****', userId)
    return this.http.put(environment.apiUrl + 'updateClient/'+ check+'/'+id,'')
    .pipe(
      map(res=>{
          return res
        }))
  }

  createDirectory(company,foldername,id){
    return this.http.post(environment.apiUrl + 'createDirectory/' + company+'/'+foldername+'/'+id,'')
    .subscribe(res => {
      console.log('res', res)
    })
  }

  getCompanyName(Id){
 
    console.log('inside service',Id)
    return this.http.get(environment.apiUrl + 'getCompanyName/'+ Id)
    .pipe(
        map(res=>{
          return res
        }))
  }

  fileUpload(file, Id){
    // console.log('inside data',file)
    // console.log('inside data Id',Id)
    
    return this.http.post(environment.apiUrl + 'fileUpload/'+Id, file,)
    .pipe(
        map(res=>{
          return res
        }))
  }

  userAccess(id){
    return this.http.post(environment.apiUrl + 'userAccess/'+id,'')
    .pipe(
        map(res=>{
          return res
        }))
  }

  getCompanyImages(id){
    return this.http.post(environment.apiUrl + 'getCompanyImages/'+id,'')
    .pipe(
        map(res=>{
          return res
        }))
  }

  getCompanyFolders(id){
    return this.http.post(environment.apiUrl + 'getCompanyFolders/'+id,'')
    .pipe(
        map(res=>{
          return res
        }))
  }

}
