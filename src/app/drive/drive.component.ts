import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { from } from 'rxjs';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { element } from '@angular/core/src/render3';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-drive',
  templateUrl: './drive.component.html',
  styleUrls: ['./drive.component.css']
})
export class DriveComponent implements OnInit {

  foldername: any;
  companyname: any;
  userType: any;
  file: any;
  access:any
  images = [];
  name: [];
  apiUrl = "http://localhost:3000/"
  addImage: FormGroup;
  formData: any;
  folder:any;
  folders:any;

  constructor(private userService: UserService, private _fb: FormBuilder) { }



  ngOnInit() {
    this.userType = JSON.parse(localStorage.getItem("userLogin"));
    console.log("company_name id:", this.userType.Id);
    this.getCompanyName();
    this.doctorFormData();
    this.userAccess();
    this.getCompanyImages();
    this.getCompanyFolders();
  }

  createDirectory() {
    console.log("folderName", this.foldername);
    this.userService.createDirectory(this.companyname, this.foldername,this.userType.Id);
  }
  getCompanyName() {
    this.userService.getCompanyName(this.userType.Id)
      .subscribe(res => {
        this.companyname = res['data'];
        console.log("hii tis is harshal", this.companyname);
      });
  }


  handleFileChanged(event) {
    // this.file = event.target.files[0];
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.addImage.get('profile').setValue(file);
      // this.addImage.get('profile').setValue(file);
      // this.addImage.get('profile').setValue(file);

      console.log('add inamgekasjdlfjsdafasdf', this.addImage);
    }
    // var formData: any = new FormData();
    // formData.append("file", this.file, this.file.name);
    // formData.append('data', this.userType.Id);
    // var file = {
    //   file: this.file,
    //   filename: this.file.name
    // }
    // console.log('file formData', formData);
    this.onSubmit();

  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.addImage.value.profile);
    // formData.append('files', this.addImage.get('profile').value);
    // formData.append('Id', this.userType.Id);
    // console.log('file formData', formData);

    this.userService.fileUpload(formData, this.userType.Id)
      .subscribe(res => {
        console.log('file response', res);
      })
  }

  doctorFormData() {
    this.addImage = this._fb.group({
      profile: [''],

    })
  }

  userAccess(){
    this.userService.userAccess(this.userType.Id).subscribe(res=>{
        this.access = res['data'].Write_access;
        console.log("write access",this.access);
    });
  };

  getCompanyImages(){
    this.userService.getCompanyImages(this.userType.Id).subscribe(res=>{
      // res['data'].forEach(element => {
      //   element.forEach(ele => {
      //     this.images.push(ele)
      //   })
      //   // console.log("company images",element);

      // })
      this.images = res['data'][1];
      this.folder = res['data'][0][0];
      console.log("company images",this.images);
      console.log("company folder",this.folder);
    
  });
  }

  getCompanyFolders(){
    this.userService.getCompanyFolders(this.userType.Id).subscribe(res=>{
      // res['data'].forEach(element => {
      //   element.forEach(ele => {
      //     this.images.push(ele)
      //   })
      //   // console.log("company images",element);

      // })
      this.name = res['data'][1];
      this.folders = res['data'][0][0];
      // console.log("company folders",this.name);
      // console.log("company folder",this.folders);
    
  });
  }

}
