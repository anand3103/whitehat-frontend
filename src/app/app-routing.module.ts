import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component'
import { from } from 'rxjs';
import { LoginComponent } from './login/login.component'
import { ClientComponent } from './client/client.component'
import { UserComponent } from './user/user.component'
import {AddclientComponent} from './addclient/addclient.component'
import { AdduserComponent } from './adduser/adduser.component'
import { AuthGuard } from './auth.guard';
import { DriveComponent } from './drive/drive.component';
import { CommonComponent } from '../app/common/common.component';

const routes: Routes = [

  { path: 'dashboard', canActivate :[AuthGuard], component:  DashboardComponent },
  { path: '', component:  LoginComponent },
  { path: 'user', canActivate :[AuthGuard], component:  UserComponent },
  { path: 'client', canActivate :[AuthGuard], component:  ClientComponent },
  { path: 'addclient', canActivate :[AuthGuard], component: AddclientComponent},
  { path: 'adduser', canActivate :[AuthGuard], component: AdduserComponent},
  { path: 'drive', canActivate :[AuthGuard], component: DriveComponent},
  { path: 'common/:name', canActivate :[AuthGuard], component: CommonComponent}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
